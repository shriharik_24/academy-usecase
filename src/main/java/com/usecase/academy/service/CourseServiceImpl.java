package com.usecase.academy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usecase.academy.model.Course;
import com.usecase.academy.repository.CourseRepository;

@Service
public class CourseServiceImpl implements CourseService{
	
	@Autowired
	private CourseRepository courseRepository;
	
	
	public List<Course> getAllCourses() {
		return courseRepository.findAll();
	}

	public Course getOneCourse(Long courseId) {
		return courseRepository.findById(courseId).orElse(new Course());
	}

	public String addCourse(Course course) {
		courseRepository.save(course);
		return "Course added successfully";
	}

	public String updateCourse(Course course) {
		courseRepository.save(course);
		return "Course details updated successfully";
	}

	public boolean deleteCourse(Long courseId) {
		courseRepository.deleteById(courseId);
		return true;
	}

}
