package com.usecase.academy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usecase.academy.model.Faculty;
import com.usecase.academy.repository.FacultyRepository;

@Service
public class FacultyServiceImpl implements FacultyService {
	
	@Autowired
	private FacultyRepository facultyRepository;
	
	public List<Faculty> getAllFaculties() {
		return facultyRepository.findAll();
	}

	public Faculty getOneFaculty(Long facultyId) {
		return facultyRepository.findById(facultyId).orElse(new Faculty());
		
	}

	public String addFaculty(Faculty faculty) {
		facultyRepository.save(faculty);
		return "Faculty added successfully";
	}

	public String updateFaculty(Faculty faculty) {
		facultyRepository.save(faculty);
		return "Faculty details updated successfully";
	}

	public boolean deleteFaculty(Long facultyId) {
		facultyRepository.deleteById(facultyId);
		return true;
	}

}
