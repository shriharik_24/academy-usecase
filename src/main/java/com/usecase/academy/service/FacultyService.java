package com.usecase.academy.service;

import java.util.List;

import com.usecase.academy.model.Faculty;

public interface FacultyService {

	public List<Faculty> getAllFaculties();

	public Faculty getOneFaculty(Long facultyId);

	public String addFaculty(Faculty faculty);

	public String updateFaculty(Faculty faculty);

	public boolean deleteFaculty(Long facultyId);

}
