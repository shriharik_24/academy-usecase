package com.usecase.academy.service;

import java.util.List;

import com.usecase.academy.model.FeesStructure;

public interface FeesService {

	public List<FeesStructure> getAllFees();

	public FeesStructure getOneFeeDetail(Long feesId);

	public String addFees(FeesStructure feesStructure);

	public String updateFees(FeesStructure feesStructure);

	public boolean deleteFees(Long feesId);

}
