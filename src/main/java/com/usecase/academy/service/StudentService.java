package com.usecase.academy.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.usecase.academy.model.Student;

public interface StudentService {
	public List<Student> getAllStudents();

	public String addStudents(Student student);

	public ResponseEntity<String> updateStudent(Student student);
	
	public boolean deleteStudent(Long studId);

	public Student getOneStudent(Long studId);

}

