package com.usecase.academy.service;

import java.util.List;

import com.usecase.academy.model.Course;

public interface CourseService {

	public List<Course> getAllCourses();

	public Course getOneCourse(Long courseId);

	public String addCourse(Course course);

	public String updateCourse(Course course);

	public boolean deleteCourse(Long courseId);

}
