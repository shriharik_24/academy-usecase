package com.usecase.academy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usecase.academy.model.FeesStructure;
import com.usecase.academy.repository.FeesRepository;


@Service
public class FeesServiceImpl implements FeesService {
	
	@Autowired
	private FeesRepository feesRepository;
	
	public List<FeesStructure> getAllFees() {
		return feesRepository.findAll();
	}

	public FeesStructure getOneFeeDetail(Long feesId) {
		return feesRepository.findById(feesId).orElse(new FeesStructure());
	}

	public String addFees(FeesStructure feesStructure) {
		feesRepository.save(feesStructure);
		return "Fees added successfully";
	}

	public String updateFees(FeesStructure feesStructure) {
		feesRepository.save(feesStructure);
		return "Fees updated successfully";
	}

	public boolean deleteFees(Long feesId) {
		feesRepository.deleteById(feesId);
		return true;
	}

}
