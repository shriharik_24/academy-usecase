package com.usecase.academy.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.usecase.academy.model.Student;

import com.usecase.academy.repository.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService {
	@Autowired
	private StudentRepository studentRepository;

	

	public String addStudents(Student student) {
		studentRepository.save(student);
		return "Students added successfully";

	}

	public List<Student> getAllStudents() {
		return studentRepository.findAll();
	}


//	public Student getOneStudent(Long id) {
//		return studentRepository.findById(id).orElse(new Student());
//	}
	
	public Student getOneStudent(Long studId) {
		return studentRepository.findById(studId).orElse(null);
	}

	public ResponseEntity<String> updateStudent(Student student) {
		studentRepository.save(student);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	public boolean deleteStudent(Long studId) {
		studentRepository.deleteById(studId);
		return true;
	}

	
	

	

}