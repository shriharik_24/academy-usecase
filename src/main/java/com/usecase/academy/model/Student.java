package com.usecase.academy.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@JsonProperty("studId")
	private Long studId;
	@Length(min=3, max= 20)
	private String studName;
	@ManyToOne(targetEntity=Course.class)
	@JoinColumn(name = "refCourseId", referencedColumnName = "courseId")
	private Course refCourseId;
	private String feesStatus;

}
