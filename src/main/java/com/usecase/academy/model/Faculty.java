package com.usecase.academy.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Faculty {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@JsonProperty("facultyId")
	private Long facultyId;
	@Length(min=3, max= 20)
	private String facultyName;
	@Length(min=2, max= 20)
	private String facultyDept;
	
}

