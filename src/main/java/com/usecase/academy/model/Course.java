package com.usecase.academy.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Course {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@JsonProperty("courseId")
	private Long courseId;
	@Length(min=1, max= 20)
	private String courseName;
	@ManyToOne(targetEntity=Faculty.class)
	@JoinColumn(name = "FacultyId", referencedColumnName = "facultyId")
	private Faculty FacultyId;
	
}
