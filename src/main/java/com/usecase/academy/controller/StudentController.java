package com.usecase.academy.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usecase.academy.config.StudentNotFoundException;
import com.usecase.academy.model.Course;
import com.usecase.academy.model.Student;
import com.usecase.academy.repository.StudentRepository;
import com.usecase.academy.service.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController {

	@Autowired
	StudentService studentService;
	@Autowired
	private StudentRepository studentRepository;

	@GetMapping("/all")
	public List<Student> getAllStudents() {
		return studentService.getAllStudents();
	}

//	@GetMapping(value = "/{studId}")
//	public Student getOneStudent(@PathVariable Long studId) {
//		return studentService.getOneStudent(studId);
//	}
	
	@GetMapping("/{studId}")
	public Student getOneStudent(@PathVariable Long studId) {
		if(!studentRepository.existsById(studId)) throw new StudentNotFoundException();
		return studentService.getOneStudent(studId);

	}

	

	@PostMapping("/add")
	// http://localhost:2346/faculty/add
	public ResponseEntity addStudents(@Valid @RequestBody Student student, BindingResult result) {
		if (result.hasErrors()) {

			List<String> errList = new ArrayList<>();
			for (ObjectError str : result.getAllErrors()) {
				errList.add(str.toString());
			}

			return new ResponseEntity<>(errList, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(studentService.addStudents(student), HttpStatus.CREATED);

	}


	
	@PutMapping("/")
	public ResponseEntity<String> updateStudent(@RequestBody Student student) {
		if(!studentRepository.existsById((student.getStudId()))) throw new StudentNotFoundException();
		studentService.updateStudent(student);
		return new ResponseEntity<>("Student updated successfully", HttpStatus.OK);

	}
	
	
	

	@DeleteMapping(value = "/delete/{studId}")
	public String deleteStudentById(@PathVariable Long studId) {
		try {
			if (studentService.deleteStudent(studId)) {
				return "Deleted Successfully";
			}
		} catch (Exception e) {
		}
		return "Not able to delete the customer";
	}

}
