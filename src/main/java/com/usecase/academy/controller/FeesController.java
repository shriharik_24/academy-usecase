package com.usecase.academy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usecase.academy.model.FeesStructure;
import com.usecase.academy.service.FeesService;

@RestController
@RequestMapping("/fees")
public class FeesController {

	@Autowired
	FeesService feesService;

	@GetMapping("/all")
	public List<FeesStructure> getAllFees() {
		return feesService.getAllFees();
	}

	@GetMapping(value = "/{feesId}")
	// http://localhost:1234/customer/get?id=10
	// http://localhost:1234/customer/get?id=10&firstName=A - multiple variables
	public FeesStructure getOneFeeDetail(@PathVariable Long feesId) {
		return feesService.getOneFeeDetail(feesId);
	}

	@PostMapping("/add")
	public String addFees(@RequestBody FeesStructure feesStructure) {
		return feesService.addFees(feesStructure);

	}

	@PutMapping("/update")
	public String updateFees(@RequestBody FeesStructure feesStructure) {
		return feesService.updateFees(feesStructure);

	}

	@DeleteMapping(value = "/delete/{feesId}")
	public String deleteFeesById(@PathVariable Long feesId) {
		try {
			if (feesService.deleteFees(feesId)) {
				return "Deleted Successfully";
			}
		} catch (Exception e) {
		}
		return "Not able to delete the customer";
	}

}
