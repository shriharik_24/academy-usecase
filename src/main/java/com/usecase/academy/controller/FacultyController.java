package com.usecase.academy.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usecase.academy.model.Faculty;
import com.usecase.academy.service.FacultyService;


@RestController
@RequestMapping("/faculty")
public class FacultyController {
	
	@Autowired
	FacultyService facultyService;
	
	


	
	@GetMapping("/all")
	public List<Faculty> getAllFaculties() {
		return facultyService.getAllFaculties();
	}
	
	@GetMapping(value = "/{facultyId}")
	// http://localhost:1234/customer/get?id=10
	// http://localhost:1234/customer/get?id=10&firstName=A - multiple variables
	public Faculty getOneFaculty(@PathVariable Long facultyId) {
		return facultyService.getOneFaculty(facultyId);
	}

	
	
	@PostMapping("/add")
	public ResponseEntity addFaculty(@Valid @RequestBody Faculty faculty, BindingResult result) {
	if (result.hasErrors()) {

	List<String> errList = new ArrayList<>();
	for (ObjectError str : result.getAllErrors()) {
	errList.add(str.toString());
	}

	return new ResponseEntity<>(errList, HttpStatus.BAD_REQUEST);
	}
	return new ResponseEntity<> (facultyService.addFaculty(faculty), HttpStatus.CREATED);

	}
	
	

	@PutMapping("/update")
	public String updateFaculty(@RequestBody Faculty faculty) {
		return facultyService.updateFaculty(faculty);

	}
	
	@DeleteMapping(value = "/delete/{facultyId}")
	public String deleteFacultyById(@PathVariable Long facultyId) {
		try {
			if (facultyService.deleteFaculty(facultyId)) {
				return "Deleted Successfully";
			}
		} catch (Exception e) {
		}
		return "Not able to delete the customer";
	}

}
