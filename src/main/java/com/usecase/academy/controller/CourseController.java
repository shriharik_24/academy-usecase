package com.usecase.academy.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usecase.academy.model.Course;
import com.usecase.academy.service.CourseService;

@RestController
@RequestMapping("/course")
public class CourseController {

	@Autowired
	CourseService courseService;

	@GetMapping("/all")
	public List<Course> getAllCourses() {
		return courseService.getAllCourses();
	}

	@GetMapping(value = "/{courseId}")
	// http://localhost:1234/customer/get?id=10
	// http://localhost:1234/customer/get?id=10&firstName=A - multiple variables
	public Course getOnecourse(@PathVariable Long courseId) {
		return courseService.getOneCourse(courseId);
	}

	@PostMapping("/add")
	// http://localhost:2346/faculty/add
	public ResponseEntity addCourse(@Valid @RequestBody Course course, BindingResult result) {
		if (result.hasErrors()) {

			List<String> errList = new ArrayList<>();
			for (ObjectError str : result.getAllErrors()) {
				errList.add(str.toString());
			}

			return new ResponseEntity<>(errList, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(courseService.addCourse(course), HttpStatus.CREATED);

	}

	@PutMapping("/update")
	public String updateCourse(@RequestBody Course course) {
		return courseService.updateCourse(course);

	}

	@DeleteMapping(value = "/delete/{courseId}")
	public String deleteCourseById(@PathVariable Long courseId) {
		try {
			if (courseService.deleteCourse(courseId)) {
				return "Deleted Successfully";
			}
		} catch (Exception e) {
		}
		return "Not able to delete the customer";
	}

}
