package com.usecase.academy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcademyDemoApplication {

	public static void main(String[] args) {

		SpringApplication.run(AcademyDemoApplication.class, args);
		
	}

}
