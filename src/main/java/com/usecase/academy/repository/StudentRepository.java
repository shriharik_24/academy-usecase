package com.usecase.academy.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.usecase.academy.model.Student;

public interface StudentRepository extends JpaRepository<Student,Long> {

}
