package com.usecase.academy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usecase.academy.model.FeesStructure;

public interface FeesRepository extends JpaRepository<FeesStructure, Long>{

}
