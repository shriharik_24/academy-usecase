package com.usecase.academy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usecase.academy.model.Faculty;

public interface FacultyRepository extends JpaRepository<Faculty, Long> {

}
