package com.usecase.academy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usecase.academy.model.Course;

public interface CourseRepository extends JpaRepository<Course, Long> {

}
